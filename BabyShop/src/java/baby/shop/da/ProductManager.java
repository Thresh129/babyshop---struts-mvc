/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.da;

import baby.shop.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Administrator
 */
public class ProductManager {

    private static  PreparedStatement searchByNameStaement;
    private static  PreparedStatement searchByIdStatement;
    
    private PreparedStatement getSearchByNameStatement() throws ClassNotFoundException, SQLException{
        if (searchByIdStatement == null){
        Connection connection = DBConnection.getConnection();
        searchByNameStaement = connection.prepareStatement("select id, name,price,description from product where name like ?");
         }
        return searchByIdStatement;
    
        }
    private PreparedStatement getSearchByIdStatment() throws ClassNotFoundException, SQLException{
         if (searchByIdStatement == null){
         Connection connection = DBConnection.getConnection();
         searchByIdStatement = connection.prepareStatement("select name, price ,description from product where id = ?");
         }
         return searchByIdStatement;
    
    }
      
    public List<Product> getProductsByName(String keyword)
    {
        try {
            PreparedStatement statement =  getSearchByNameStatement();
            statement.setString(1, "%" + keyword + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> product = new LinkedList<Product>();
            while (rs.next()){
            int id = rs.getInt("id");
            String name = rs.getString("name");
            float price = rs.getFloat("price");
            String description = rs.getString("description");
            product.add(new Product(id, name, price, description));
            
            }
            return product;
            
        }catch(Exception ex){
      //  logger.getlogger(baby.shop.da.ProductManager,class.getName()).log(level.SEVERE,null,ex);
        return new LinkedList<Product>();
        }
        }
    
    
    public Product getProductById(int id) throws ClassNotFoundException, SQLException{
          try{
              PreparedStatement statement = getSearchByIdStatment();
              statement.setInt(1,id);
              ResultSet rs = statement.executeQuery();
              while(rs.next()){
              String name = rs.getString("name");
              float price =rs.getFloat("price");
              String desrcription = rs.getNString("description");
              return new Product (id, name, price, description());
              }
          }catch(ClassNotFoundException | SQLException ex){
          Logger.getLogger(baby.shop.da.ProductManager.class.getName()).log(Level.SEVERE, null,ex);
          }
          return new Product(0,"",0,"");


}

    public List<Product> getProductByName(String keyword) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String description() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}